# htmlToXBel

htmlToXBel - a tool to convert bookmarks stored in the html format into the xbel format.

This is part of the [Endorphin browser](https://gitlab.com/EndorphinBrowser/browser), but is maintained here.

